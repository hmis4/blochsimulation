%% Excitation pulse initialisation
%Initialise the pulse parameters 
points = 10001; %use the same vector length for each part (excitation, delay, and BS)
pulsedur = 1e-3; %in seconds

%Simulate excitation pulse vector
%To choose the relevant excitation pulse, uncomment or add the function for
%the excitation pulse desired
%exciteVec = calcSinc('n', 8, 'R', 40, 'Tp', pulsedur*10^3, 'nPoints', points, 'plot',false, 'freq', 0); %pulse duration is in ms in this function
exciteVec = calcHard('Tp', pulsedur*10^3, 'nPoints', points); %convert pulse duration in ms
B1Excite = exciteVec.B1hard.*exp(1i*exciteVec.phases)

%include off-resonant frequency effects
offset_freq = 0; %choosing no frequency offset here
radShift = offset_freq * pulsedur *2*pi;
offPhase = linspace(0,radShift,points).'; %the phase shift depending on the time interval
excPulse = B1Excite .* exp(1i*offPhase).';

%% Bloch-Siegert Fermi pulse intialisation
%Initialise pulse parameter (extracted from the optimal pulse as given in
%paper Clarke et al.
fermiDur = 3.5e-3; %in seconds (copied from the paper)
a = 224e-3; %203e-3%0.064*fermiDur*10^3 % in ms, optimised, as in paper
T0 = 875e-3; %787e-3%0.250*fermiDur*10^3 %in ms, optimised, as in paper

%calculate the BS pulse vector
BSVec = calcFermi('Tp', fermiDur*10^3, 'nPoints', points, 't0', T0, 'a', a); %convert pulse duration into ms, will also plot the pulse

%set up Fermi pulse relation to excitation pulse
BSdelay = 0; %in seconds, here assume no delay between excitation and Fermi 
BSdelayvec = linspace(0,BSdelay, points);
%(actually decide to neglect this later on in the Bloch simulation, but can
%add back in in B1_tot if needed into the Bloch simulation)

disp('Check1')

%% Bloch simulation for B1 amplitude
B1exc_max = 0 % set to zero amplitude for simulating Fermi pulse only. 0:10:1000 % in Hz amplitude of the excitation pulse, currently assuming fixed excitation pulse amplitude whilst changing the other parameters
B1BS_max = linspace(0,2*pi*100); % in Hz amplitude of the Bloch Siegert pulse

clear endPhase
clear Mxyout
clear Mzout
for i = 1:length(B1BS_max)
    %excitation B1
    t = exciteVec.t./1000; %rescale into units of seconds (is this needed for bloch_CTR_Hz comparison?)
    %B1vec = B1_max(i)*exciteVec.B1sinc
    B1exc = B1exc_max(i)*excPulse;
    
    %add the phase shift
    BS_offres = 1000; %in Hz
    FermiRadShift = BS_offres * fermiDur * 2*pi; % this is the total fermi phase 
    
    %Fermi B1
    t_BS = BSVec.t;
    BSvec = B1BS_max(i)*BSVec.B1fermi.'.*exp(1i*linspace(0,FermiRadShift,length(t_BS)));
    
    %total B1 applied
    B1tot = [B1exc,zeros(size(BSdelayvec)), BSvec]; % for hard pulse simulation [BSvec,zeros(size(BSdelayvec)), B1exc];
    
    %setup parameters for Bloch simulation
    tp = [(t(2)-t(1))*ones(size(B1exc)), (BSdelayvec(2)-BSdelayvec(1))*ones(size(BSdelayvec)), (t_BS(2)-t_BS(1))*ones(size(BSvec))];
    %for hard pulse simulation: tp = [(t_BS(2)-t_BS(1))*ones(size(BSvec)), (BSdelayvec(2)-BSdelayvec(1))*ones(size(BSdelayvec)), (t(2)-t(1))*ones(size(B1exc))];
    gr = zeros(size(B1tot));
    df = 0;
    dp = 0;
    mode = 2;
    mx = 0;
    my = 0;
    mz = 1;

    %run the Bloch simulation
    [outMx,outMy,outMz] = bloch_CTR_Hz(B1tot,gr,tp,3,0.2,df,dp,mode,mx,my,mz);
    Mxy = abs(outMx + 1i*outMy); 
    argMxy = angle(outMx+1i*outMy);
    Mxyout(i)= Mxy(length(tp));  
    Mzout(i) = outMz(length(tp));
    %Mxyout(i,:) = Mxy(length(t),:);
    %Mzout(i,:) = outMz(length(t),:);
    endPhase(i) = 180 * argMxy(length(tp))/pi;
end

disp('Check2')

%% Plots
%plot Mxy vs B1
figure(3);
hold on
plot(B1BS_max, Mxyout, 'DisplayName','Mxy simulated');
plot(B1BS_max, Mzout, 'DisplayName', 'Mz simulated');
xlabel('B1_{max} in Hz'); %in Hz
ylabel('Magnetisation Component');
title('End of pulse excitation for Fermi pulse');
legend show
hold off

figure(4);
hold on
plot(B1BS_max, endPhase, 'DisplayName','Phase simulated');
xlabel('B1_{max} in Hz'); %in Hz
ylabel('Phase of Magnetisation/Degrees');
title('End of pulse phase of magnetisation for Fermi pulse');
legend show
hold off


%% Bloch simulation for wRF
%note "B1" really refers to gamma*B1 .... I think!
B1exc_max = 0; %for Fermi pulse simulation only, need no excitation amplitude % in Hz amplitude of the excitation pulse, currently assuming fixed excitation pulse amplitude whilst changing the other parameters
B1BS_max = 200; % in Hz amplitude of the Bloch Siegert pulse
wRFoffset = linspace(-10000, 10000,1000); %in Hz

%TO DO: add in subtraction of end phase when no fermi pulse applied to get
%phase SHIFT rather than absolute phase

clear endPhase
clear Mxyout
clear Mzout
for i = 1:length(wRFoffset)
    %excitation vector
    t = exciteVec.t./1000; %rescale into units of seconds (is this needed for bloch_CTR_Hz comparison?)
    %B1vec = B1_max(i)*exciteVec.B1sinc
    B1exc = B1exc_max*excPulse;
    
    %add the phase shift (should have the same effect as having df vector, but should check this)
    BS_offres = wRFoffset(i); %in Hz
    FermiRadShift = BS_offres * fermiDur * 2*pi; % this is the total fermi phase 
    
    %Fermi pulse vectors
    t_BS = BSVec.t/1000;
    BSvec = B1BS_max*BSVec.B1fermi.'.*exp(1i*linspace(0,FermiRadShift,length(t_BS)));
    
    B1tot = [B1exc,zeros(size(BSdelayvec)), BSvec];
    
    %setup parameters for Bloch simulation
    tp = [(t(2)-t(1))*ones(size(B1exc)), (BSdelayvec(2)-BSdelayvec(1))*ones(size(BSdelayvec)), (t_BS(2)-t_BS(1))*ones(size(BSvec))];
    gr = zeros(size(B1tot));
    df = 0;
    dp = 0;
    mode = 2;
    mx = 0;
    my = 0;
    mz = 1;

    %run the Bloch simulation
    [outMx,outMy,outMz] = bloch_CTR_Hz(B1tot,gr,tp,3,0.2,df,dp,mode,mx,my,mz);
    Mxy = abs(outMx + 1i*outMy); 
    argMxy = angle(outMx+1i*outMy);
    Mxyout(i)= Mxy(length(tp));  
    Mzout(i) = outMz(length(tp));
    %Mxyout(i,:) = Mxy(length(t),:);
    %Mzout(i,:) = outMz(length(t),:);
    endPhase(i) = 180 * argMxy(length(tp))/pi;
end

disp('Check2')

%% Plots
%plot Mxy vs B1
figure(5);
hold on
plot(wRFoffset, Mxyout, 'DisplayName','a=224us, T0=875us'); %change DisplayName as necessary for the simulated pulse
%plot(wRFoffset, Mzout, 'DisplayName', 'Mz simulated');
xlabel('wRF in Hz'); %in Hz
ylabel('Magnetisation Component');
title('End of pulse excitation for Fermi pulse');
legend show
hold off

figure(6);
hold on
plot(wRFoffset, endPhase, 'DisplayName','Phase simulated');
xlabel('wRF in Hz'); %in Hz
ylabel('Phase of Magnetisation/Degrees');
title('End of pulse phase of magnetisation for Fermi pulse');
legend show
hold off

