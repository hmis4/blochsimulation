% See if the current Bloch simulator simulates the Bloch Siegert effect


%% Excitation pulse 
%Load the MDR 7T pulse
%load('D:\Will\Documents\MATLAB\will\SeqSim\MDR3TPulse.mat')
duration.excite = 1.010e-3; % Units: s.
timeResExcite = 2E5; % Points per s
numPoints.excite = duration.excite * timeResExcite;

pulseDataInterp = interp1(1:numel(pulseData),pulseData,linspace(1,numel(pulseData),numPoints.excite));

% Estimate the FA achieved with the MDR pulse
calcAmplitudeHz = @(FA,dur) FA/360 * 1/dur;
B1Excite =  pulseDataInterp.' * calcAmplitudeHz(30,0.0001919);

B1Excite = ones(numPoints.excite);

%% Shift it
offset_freq = 0; 
radShift = offset_freq * duration.excite *2*pi;
offPhase = linspace(0,radShift,numPoints.excite).';
excitationPulse = B1Excite .* exp(1i*offPhase);

%% Bloch sigert pulse

FERMI = @(wRF,t,t0,a) exp(1i*wRF*t)./(1 + exp((abs(t)-t0)./a));

fermiDuration = 0.008;
fermiTimePoint = 1e-6;
FermiTp = (-fermiDuration/2):fermiTimePoint:(fermiDuration/2);

fermiB1 = FERMI(0,FermiTp,0.002300,0.000630).';


%% loop power
powerVec = 0:50:500;
parfor pDx = 1:numel(powerVec)
    
    % Shift it off resonance
    BS_offres = 1000;
    FermiRadShift = BS_offres * fermiDuration * 2*pi;
    FermiOffPhase = linspace(0,FermiRadShift,numel(FermiTp)).';
    
    % pulse power
    BSHz = powerVec(pDx);
    BSPulse = BSHz * fermiB1 .* exp(1i*FermiOffPhase);

    exciteVec = [excitationPulse ;BSPulse];
    gr = zeros(size(exciteVec));
    tp = [1/timeResExcite * ones(size(B1Excite)) ; fermiTimePoint * ones(size(fermiB1)) ];
    df = 0;
    dp = 0;
    mode = 2;
    mx = 0;
    my = 0;
    mz = 1;

    [outMx,outMy,outMz] = bloch_CTR_Hz(exciteVec,gr,tp,3,0.2,df,dp,mode,mx,my,mz);

%     figure(3455)
%     clf
%     plot(df,outMz(end,:))
%     hold on
%     plot(df,abs(outMx(end,:)+1i*outMy(end,:)),'r')
% 
% 
%     figure(3456)
%     clf
%     plot(cumsum(tp),outMz(:,3001))
%     hold on
%     plot(cumsum(tp),abs(outMx(:,3001)+1i*outMy(:,3001)),'r')

    % Phase at 0 Hz
    endPhase(pDx) = 180 * angle(outMx(end,1)+1i*outMy(end,1))/pi;

end
figure(5677);
clf
plot(powerVec,endPhase)

%% Loop the frequency 

offresVec = [-10000 -5000 -4000 -3000 -2000 -1750 -1500 -1250 -1000 -800 -600 -400 -200 0 200 400 600 800 1000 1250 1500 1750 2000 3000 4000 5000 10000];
parfor oDx = 1:numel(offresVec)
    
    % Shift it off resonance
    BS_offres = offresVec(oDx);
    FermiRadShift = BS_offres * fermiDuration * 2*pi;
    FermiOffPhase = linspace(0,FermiRadShift,numel(FermiTp)).';
    
    % pulse power
    BSHz = 276;
    BSPulse = BSHz * fermiB1 .* exp(1i*FermiOffPhase);

    exciteVec = [excitationPulse ;BSPulse];
    gr = zeros(size(exciteVec));
    tp = [1/timeResExcite * ones(size(B1Excite)) ; fermiTimePoint * ones(size(fermiB1)) ];
    df = 0;
    dp = 0;
    mode = 2;
    mx = 0;
    my = 0;
    mz = 1;

    
    [outMx,outMy,outMz] = bloch_CTR_Hz(exciteVec,gr,tp,3,0.2,df,dp,mode,mx,my,mz);

    figure(3400+oDx)
    clf
    plot(df,outMz(end,:))
    hold on
    plot(df,abs(outMx(end,:)+1i*outMy(end,:)),'r')
    title(sprintf('Frequency = %0.0f',offresVec(oDx)));
    
    % Phase at 0 Hz
    endPhaseFreq(oDx) = 180 * angle(outMx(end,1)+1i*outMy(end,1))/pi;
    endAmpFreq(oDx) = abs(outMx(end,1)+1i*outMy(end,1));
    endMzFreq(oDx) = outMz(end,1);

end
%%
figure(5678);
clf
subplot(1,2,1)
plot(offresVec,endMzFreq)

subplot(1,2,2)
plot(offresVec,endPhaseFreq-endPhase(1))

%% Check what pulse we have been playing out on the scanner.Compare it with above.
strPath='D:\Will\Documents\MATLAB\will\Jobs 2014\BlochSiegert\SBB_WTC_Fermi_Pulse.cpp__pulse_dump_Tp=8000us_t0=2300s_a.dump'
% Check that it is symmetrical. 

expt = debugHs8InPulse(strPath,2);
B1 = expt(1,:) .*exp(1i*expt(2,:));


figure(7000)
clf;
plot(B1)
hold on 
plot(fermiB1,'r:')

% Yep they match


%% Simulate the vaious peaks as the frequency is swept
offresVec2 = -10000:20:10000;
for oDx = 1:numel(offresVec2)
    
    % Shift it off resonance
    BS_offres = offresVec2(oDx);
    FermiRadShift = BS_offres * fermiDuration * 2*pi;
    FermiOffPhase = linspace(0,FermiRadShift,numel(FermiTp)).';
    
    % pulse power
    BSHz = 276;
    BSPulse = BSHz * fermiB1 .* exp(1i*FermiOffPhase);

    exciteVec = [exciteVec; BSPulse];
    gr = zeros(size(exciteVec));
    tp = [1/timeResExcite * ones(size(B1Excite)) ; fermiTimePoint * ones(size(fermiB1)) ];
    df = [710 0 -300 -920 -1960].' + 920; % Centred on aATP
    dp = 0;
    mode = 2;
    mx = 0;
    my = 0;
    mz = 1;

    [outMx,outMy,outMz] = bloch_CTR_Hz(exciteVec,gr,tp,3,0.2,df,dp,mode,mx,my,mz);

    Mz_swept(oDx,:) = outMz(end,:);
    
    Mxy_swept(oDx,:) = abs(outMx(end,:) + 1i*outMy(end,:));
    Phase_swept(oDx,:) = angle(outMx(end,:) + 1i*outMy(end,:));
end
%%
figure(3500)
clf
plot(offresVec2,Mz_swept)
xlim([-5000 5000])
hold on

verticalLines = [-10000 -5000 -4000 -3000 -2000 -1000 1000 2000 3000 4000 5000 10000];

ylims = ylim;
for iDx = 1:numel(verticalLines)
    plot([verticalLines(iDx) verticalLines(iDx)],ylims,'k:')
end

set(gca,'xdir','reverse')

figure(3501)
clf
plot(offresVec2,Mxy_swept)
xlim([-5000 5000])
hold on

verticalLines = [-10000 -5000 -4000 -3000 -2000 -1000 1000 2000 3000 4000 5000 10000];

ylims = ylim;
for iDx = 1:numel(verticalLines)
    plot([verticalLines(iDx) verticalLines(iDx)],ylims,'k:')
end

set(gca,'xdir','reverse')

figure(3502)
clf
hold on
for iDx = 1:size(Phase_swept,2)
    plot(offresVec2,unwrap(Phase_swept(:,iDx))+(iDx-1)*6)
end
xlim([-5000 5000])
rainbowcolour


verticalLines = [-10000 -5000 -4000 -3000 -2000 -1000 1000 2000 3000 4000 5000 10000];

ylims = ylim;
for iDx = 1:numel(verticalLines)
    plot([verticalLines(iDx) verticalLines(iDx)],ylims,'k:')
end

set(gca,'xdir','reverse')

