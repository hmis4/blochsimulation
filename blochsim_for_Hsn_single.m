%% Bloch Simulation for adiabatic HSn excitation pulse (n=8)
%calculate the excitation pulse
%pulsedur = 12288e-6; %in seconds
pulsedur = 1e-3; %in seconds (1ms in the paper for T_p)
points = 10001;
A = 2*pi*25*1e3; %max frequency offset in radians per second
%fermiDur = 3.5e-3; %in seconds
%B1_max = 10000 %amplitude of the pulse in units of Hz
B1_max = linspace(0,2*pi*100);
%Mxyout = zeros(size(B1_max));
%exciteVec = calcHsn('n', 8, 'R', 18, 'Tp', pulsedur*10^3, 'nPoints', points, 'plot', false, 'freq', 0); %pulse duration is in ms in this function
%exciteVec = calcSinc('n', 8, 'R', 40, 'Tp', pulsedur*10^3, 'nPoints', points, 'plot',false, 'freq', 0); %pulse duration is in ms in this function
%exciteVec = calcBir4('n', 8, 'R', A*pulsedur/pi, 'kappa', atan(18), 'Tp', pulsedur*10^3, 'nPoints', points, 'plot', true, 'freq', 0, 'flipAngle', 90); %pulse duration is in ms in this function
exciteVec = calcHard('Tp', pulsedur*10^3, 'nPoints', points);
%exciteVec = Bir4pulseLookup()
%exciteVec = HSnpulseLookup()

BSdelay = 0 %100e-3 % in seconds
BSdelayvec = linspace(0,BSdelay, points);
BSVec = calcFermi('Tp', fermiDur*10^3, 'nPoints', points);
BS_offres = 1000; %in Hz
FermiRadShift = BS_offres * fermiDur * 2*pi;

disp('Check1')

%% plot amplitude and phase of pulses
% figure(22)
% hold on
% %plot(exciteVec.t, exciteVec.B1bir4, 'DisplayName', 'Pulse Amplitude simulated')
% plot(linspace(0, pulsedur*1e3, exciteVec.pulsePts), exciteVec.abs, 'DisplayName', 'Pulse Amplitude csifid')
% xlabel('Time/ms')
% ylabel('Amplitude, |B_1|/|B_1(0)|')
% legend show
% 
% %wrappedphase = mod(exciteVec.A*exciteVec.phases + 2*pi, 2*pi)
% %wrappedphase = mod(exciteVec.phases + 2*pi, 2*pi)
% figure(23)
% hold on
% %plot(exciteVec.t, exciteVec.phases,'DisplayName', 'Phase simulated');
% %plot(exciteVec.t, wrappedphase,'DisplayName', 'Wrapped Phase simulated');
% plot(linspace(0, pulsedur*1e3, exciteVec.pulsePts), exciteVec.pha ,'DisplayName', 'Wrapped Phase csifid');
% xlabel('Time/ms')
% ylabel('Amplitude, |B_1|/|B_1(0)|')
% legend show


%% B1 vector simulation
for i = 1:length(B1_max)
    t = exciteVec.t./1000; %rescale into units of seconds (is this needed for bloch_CTR_Hz comparison?)
    %B1vec = B1_max(i)*exciteVec.B1Hs8.*exp(1i*exciteVec.phases);
    %B1vec = B1_max(i)*exciteVec.B1sinc
    %B1vec = B1_max(i)*exciteVec.B1bir4.*exp(1i*exciteVec.phases);
    B1vec = 1000*exciteVec.B1hard.*exp(1i*exciteVec.phases);
    
    t_BS = BSVec.t;
    BSvec = B1_max(i)*BSVec.B1fermi.'.*exp(1i*linspace(0,FermiRadShift,length(t_BS)));
    
    B1tot = [B1vec, zeros(size(BSdelayvec)), BSvec];
    
    %B1vec = B1_max(i)*exciteVec.amp.*exp(1i*exciteVec.ph);
    %B1vec = B1_max(i)*exciteVec.abs.*exp(1i*exciteVec.pha);
    %t = linspace(0, pulsedur, exciteVec.pulsePts);
    
    %setup parameters
    %timeResExcite = points/pulsedur; %number of points per second (bandwidth of pulse in Hz)
    %timeResExcite = exciteVec.pulsePts/pulsedur;
    %tp = 1/timeResExcite * ones(size(B1vec)); %(t(2)-t(1))*ones(size(B1vec));
    tp = [(t(2)-t(1))*ones(size(B1vec)),(BSdelayvec(2)-BSdelayvec(1))*ones(size(BSdelayvec)), (t_BS(2)-t_BS(1))*ones(size(BSvec))];
    gr = zeros(size(B1tot));
    df = 0;
    %df = linspace(-A/(2*pi),A/(2*pi),101); %frequency offset vector
    dp = 0;
    mode = 2;
    mx = 0;
    my = 0;
    mz = 1;

    %run the Bloch simulation
    [outMx,outMy,outMz] = bloch_CTR_Hz(B1tot,gr,tp,3,0.2,df,dp,mode,mx,my,mz);
    Mxy = abs(outMx + 1i*outMy);    
    %Mxyout(i)= Mxy(length(t));  
    %Mzout(i) = outMz(length(t));
    %Mxyout(i,:) = Mxy(length(t),:);
    %Mzout(i,:) = outMz(length(t),:);
    endPhase(i) = 180 * angle(outMx(end,:)+1i*outMy(end,:))/pi;
end
disp('Check2')
%% Plots
%plot Mxy vs B1
figure(20);
hold on
%plot(B1_max, Mxyout(:,51), 'DisplayName','Mxy simulated');
%plot(B1_max, Mzout(:,51), 'DisplayName', 'Mz simulated');
plot(B1_max, Mxyout, 'DisplayName','Mxy simulated');
plot(B1_max, Mzout, 'DisplayName', 'Mz simulated');
%plot(linspace(0,B1_max(length(B1_max))), zeros(length(B1_max)), 'k')
xlabel('B1_{max} in Hz'); %in Hz?
ylabel('Magnetisation Component');
title('End of pulse excitation for Fermi pulse');
legend show
%df(51)

figure(21);
hold on
plot(B1_max, endPhase)
xlabel('B1,peak')
ylabel('end phase/degrees')

figure(22);
hold on
plot(B1_max, sqrt(endPhase))
xlabel('B1,peak')
ylabel('sqrt(end phase/degrees)')

%% Pulse profile

figure(90);
%[df, B1_max] = meshgrid(df, B1_max);
contour3(df, B1_max, Mxyout);
xlabel('Frequency offset/Hz');
ylabel('Peak B1');
zlabel('End of BIR4 Mxyout');


%%
%plot pulse profile
figure(21);
v = [0 0.2 0.3 0.4 0.5 0.6 0.7 0.8];
[C,h] = contour(df, B1_max, Mxyout,v);
clabel(C,h, 'manual')
%clabel(C,h, 'LabelSpacing', 1500)
xlabel('Frequency offset/Hz');
ylabel('Peak B1');
zlabel('End of BIR4 Mz');

%% 
figure(90);
hold on
B1 = B1_max(20)
Mxyvec = Mxyout(20,:);
plot(df, Mxyvec) 
xlabel('df')
ylabel('Mxyout')


%% Plotting graphs
%plot of RF pulse magnitude
figure(1);
hold on
subplot(2,2,1);
%axis('equal');
%plot(t, exciteVec.B1Hs8);
plot(t, exciteVec.B1sinc);
xlabel('Time/ms');
ylabel('HS8 Pulse Magnitude');
title('Magnitude of HS8 Pulse')

%plot of Mz
subplot(2,2,2);
plot(t, outMz);
xlabel('Time/ms');
ylabel('Mz');
title('Mz Magnetisation');

%plot of Mx and My
subplot(2,2,3);
plot(t, outMx);
hold on
plot(t, outMy);
legend('Mx', 'My');
xlabel('Time/ms');
ylabel('Magnetisation');
title('Mx and My evolution');
hold off

%plot of Mxy
subplot(2,2,4);
plot(t, abs(outMx + 1i*outMy));
xlabel('Time/ms');
ylabel('Mxy');
title('Transverse Magnetisation Mxy evolution');
hold off

%% Plotting graphs
%plot of RF pulse magnitude

figure(2);
hold on
%subplot(2,2,1);
%axis('equal');
plot(t, exciteVec.B1Hs8);
xlabel('Time/ms');
ylabel('HS8 Pulse Magnitude');
title('Magnitude of HS8 Pulse');
hold off

figure(3)
hold on
for j=1:length(df)
    %plot of Mz
    %subplot(2,2,2);
    if df(j) ~= 0
        plot(t, outMz(:,j), 'DisplayName', ['df = ', num2str(df(j))]);
        xlabel('Time/ms');
        ylabel('Mz');
        title('Mz Magnetisation');
    end
end
legend show
%     %plot of Mx and My
%     figure(4)
%     %subplot(2,2,3);
%     plot(t, outMx(:,j));
%     hold on
%     plot(t, outMy(:,j));
%     legend('Mx', 'My');
%     xlabel('Time/ms');
%     ylabel('Magnetisation');
%     title('Mx and My evolution');
%     hold off

    %plot of Mxy
figure(5)
hold on
for j=1:length(df)
    %subplot(2,2,4);
    if df(j) ~= 0
        plot(t, abs(outMx(:,j) + 1i*outMy(:,j)), 'DisplayName', ['df = ', num2str(df(j))]);
        xlabel('Time/ms');
        ylabel('Mxy');
        title('Transverse Magnetisation Mxy evolution');
    end
end
legend show
