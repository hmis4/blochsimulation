function [out] = calcBir4(varargin)
% Matlab reference implementation of BIR4 pulses 
%
% Syntax:
% 'R' , time-bandwidth product
% calcHsn('R' , 20), default is 40
%
% 'Tp' , duration (in ms)
% calcHsn('Tp',20), default is 10
%
% 'zeta' , value for the truncation factor zeta
% calcHsn('beta', acosh(100)), default is 10
%
% 'kappa', value
% calc
%
% 'nPoints' , integer number of datapoints in output
% calcHsn('nPoints', 1000), default is 10001 - MUST BE AN ODD NUMBER
%
% 'plot' , boolean (true or false) whether to display the pulse
% calcHsn('plot', true), default is true
%
% 'freq' , offset frequency of pulse (Hz)
% calcHsn('freq', 100), default is 0Hz
%
% EXAMPLE:
%
% Calling syntax is to pass any number of these as name, value pairs
% E.g. Glue a series of these together.  Default would be
% calcHsn('n' ,8,'R',40,'Tp',20,'beta',acosh(100),'nPoints',10001,'plot',true,'freq',0)
%
% or
%
% Pass a struct with the options as fields.

%
% Copyright Chris Rodgers, University of Oxford, 2010-12.
% $Id$

opt = processVarargin(varargin{:});

if ~isfield(opt,'R')
    opt.R = 40; % Units: ms.
end

if ~isfield(opt,'Tp')
    opt.Tp = 10; % Units: ms.
end

if ~isfield(opt,'zeta')
    opt.zeta = 10 % For 1% truncation level (i.e. 1/1% = 100)
end

if ~isfield(opt,'kappa')
    opt.kappa = atan(10); % For 1% truncation level (i.e. 1/1% = 100)
end

if ~isfield(opt,'flipAngle')
    opt.flipAngle = 90; %Flip angle 90deg excitation
end
    
if ~isfield(opt,'nPoints')
    opt.nPoints = 10001;
end

if ~isfield(opt,'plot')
    opt.plot = true;
end

isfield(opt , 'freq' )

if ~isfield(opt,'freq')
    opt.freq = 0;
end

A = pi* opt.R / opt.Tp;        % I.e. \Delta\omega_{max} in de Graaf's notation. Units of kHz.

tau = linspace(0,1,opt.nPoints); % Scaled time.
t   = [tau, tau+1, tau+2, tau+3] * opt.Tp/4; % Time in same units as Tp.

% Main Reference:
%
% Secondary Reference

% This is the magnitude of the B1 in a BIR-4 pulse. There are 4 segments to
% the BIR4 pulse: 

AHP = tanh(opt.zeta * tau);
revAHP = tanh(opt.zeta *(1-tau));

B1bir4 = [revAHP, AHP, revAHP, AHP];

figure();plot(t, B1bir4)

%calculate DeltaOmega(t)
freq_offset = @(tau) -A*tan(opt.kappa*(1-tau))/tan(opt.kappa); %frequency offset is in radians per s 
revfreq_offset = @(tau) A*tan(opt.kappa*(tau))/tan(opt.kappa);% QUESTION: is the tan meant to be calculating in degrees instead of radians??

%freq_offs = [revfreq_offset, freq_offset, revfreq_offset, freq_offset];
%figure();plot(t, freq_offs)
%omega = @(scaledtime) omega_max*tan(opt.kappa*(1-scaledtime))/tan(opt.kappa);

%calculate the phases by integrating
for i=1:length(tau)
    phaseoff(i) = opt.Tp/4*integral(@(tau) freq_offset(tau),0, tau(i)); %phase offset is in radians
end

for i=1:length(tau)
    revphaseoff(i) = opt.Tp/4 * integral(@(tau) revfreq_offset(tau),0, tau(i));
end

%phases in each segement of the BIR4 pulse
phase1 = revphaseoff;
phase2 = phase1(length(tau)) + phaseoff + (180+opt.flipAngle/2)*pi/180; %include the offset to achieve FA = 90deg
phase3 = phase2(length(tau)) + revphaseoff; %include offset to achieve FA = 90deg
phase4 = phase3(length(tau)) + phaseoff -(180+opt.flipAngle/2)*pi/180;%get rid off the offset

%concatenate the phases
phases = [phase1, phase2, phase3, phase4];

figure();plot(t,phases)

if nargout > 0
    vars = whos();
    for idx=1:numel(vars)
        out.(vars(idx).name) = eval(vars(idx).name);
    end
end
