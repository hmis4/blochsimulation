function [out] = calcFermi(varargin)
%Implementing the Bloch Siegert pulse for B1 mapping using a Fermi Pulse
%(pulled code from WTC's code)

%Extract the relevant parameters
opt = processVarargin(varargin{:});

if ~isfield(opt,'wRF')
    opt.wRF = 0; % Units: rad/s.
end

if ~isfield(opt,'Tp')
    opt.Tp = 3.5; % Units: ms.
end

if ~isfield(opt,'t0')
    opt.t0 = 0.875; % Units: ms.
end

if ~isfield(opt, 'a')
    opt.a = 0.224; % Units:ms.
end

if ~isfield(opt,'nPoints')
    opt.nPoints = 1000;
end

if ~isfield(opt,'plot')
    opt.plot = true;
end

%Set up the time vector
tau = linspace(-opt.Tp/2,opt.Tp/2, opt.nPoints);
t = tau + opt.Tp/2;

%Fermi pulse function with normalised amplitude (peak amplitude = 1)
FERMI = @(wRF,t,t0,a) exp(1i*wRF*t)./(1 + exp((abs(t)-t0)./a)); %wRF is the frequency shift gamma*B_0

B1fermi = FERMI(opt.wRF,tau,opt.t0, opt.a).'; %B1 out for given parameters (amplitude and phase included)

%output the relevant parameters
if nargout > 0
    vars = whos();
    for idx=1:numel(vars)
        out.(vars(idx).name) = eval(vars(idx).name);
    end
end


if ~opt.plot
    return
end

figure(10);
hold on
plot(t, B1fermi) %'DisplayName', 'a = 224us, T0 = 875us')
xlabel('time/s')
ylabel('Normalised B1 pulse shape')
title('Fermi Pulse')
legend show
hold off



