function [out] = calcHard(varargin)

opt = processVarargin(varargin{:});

if ~isfield(opt,'Tp')
    opt.Tp = 10; % Units: ms.
end

if ~isfield(opt,'nPoints')
    opt.nPoints = 10001;
end

if ~isfield(opt,'plot')
    opt.plot = true;
end

if ~isfield(opt,'freq')
    opt.freq = 0;
end

tau = linspace(0,1,opt.nPoints); % Scaled time.
t = tau * opt.Tp; % Time in same units as Tp.

B1hard = ones(size(t));
phases = zeros(size(t));

if nargout > 0
    vars = whos();
    for idx=1:numel(vars)
        out.(vars(idx).name) = eval(vars(idx).name);
    end
end

if ~opt.plot
    return
end

figure(1);
plot(t, B1hard)
xlabel('time/ms')
ylabel('Normalised B1 magnitude')
title('Hard Pulse RF magnitude')

figure(2);
plot(t, phases)
xlabel('time/ms')
ylabel('B1 phase')
title('Hard Pulse RF phase')
