function [out] = calcHsn(varargin)
% Matlab reference implementation of HSn pulses for use in testing
% the 7T ShMOLLI sequence code.
%
% Syntax:
% 'n' , integer (this is the integer of HSn, e.g. 1, 4, 8 etc.
% calcHsn('n' , 1), default is 8
%
% 'R' , time-bandwidth product
% calcHsn('R' , 20), default is 40
%
% 'Tp' , duration (in ms)
% calcHsn('Tp',20), default is 10
%
% 'beta' , value for the truncation factor beta
% calcHsn('beta', acosh(100)), default is cosh(100) - i.e. a 1% truncation
%
% 'nPoints' , integer number of datapoints in output
% calcHsn('nPoints', 1000), default is 10001 - MUST BE AN ODD NUMBER
%
% 'plot' , boolean (true or false) whether to display the pulse
% calcHsn('plot', true), default is true
%
% 'freq' , offset frequency of pulse (Hz)
% calcHsn('freq', 100), default is 0Hz
%
% EXAMPLE:
%
% Calling syntax is to pass any number of these as name, value pairs
% E.g. Glue a series of these together.  Default would be
% calcHsn('n' ,8,'R',40,'Tp',20,'beta',acosh(100),'nPoints',10001,'plot',true,'freq',0)
%
% or
%
% Pass a struct with the options as fields.

%
% Copyright Chris Rodgers, University of Oxford, 2010-12.
% $Id$

opt = processVarargin(varargin{:});

if ~isfield(opt,'n')
    opt.n = 8;
end

if ~isfield(opt,'R')
    opt.R = 40;
end

if ~isfield(opt,'Tp')
    opt.Tp = 10; % Units: ms.
end

if ~isfield(opt,'beta')
    opt.beta = acosh(100); % For 1% truncation level (i.e. 1/1% = 100)
end

if ~isfield(opt,'nPoints')
    opt.nPoints = 10001;
end

if ~isfield(opt,'plot')
    opt.plot = true;
end

isfield(opt , 'freq' )

if ~isfield(opt,'freq')
    opt.freq = 0;
end

A = opt.R / opt.Tp;        % I.e. \Delta\omega_{max} in de Graaf's notation. Units of kHz.

tau = linspace(-1,1,opt.nPoints); % Scaled time.
t   = (tau+1) * opt.Tp / 2; % Time in same units as Tp.

% Main Reference: Tannus & Garwood: 1996.
%
% Secondary Reference, but WARNING - contains errors:
% "Implementation equations for HSn RF pulses"; Yasvir A. Tesiram;
% Journal of Magnetic Resonance 204 (2010) 333�339.

% This is the magnitude of the B1 in an HS8 pulse. Phase requires doing an
% integral numerically - see my Mathematica notebook on HS8.
B1Hs8 = sech(opt.beta * tau.^opt.n);

% Old integration just to give frequency offsets.
% [phT,F2vals] = ode45(@(tau,F2) sech(opt.beta * tau^opt.n)^2, linspace(0,1,1001),0);

% Now double-integrate to also get phases.
[phT,soln] = ode45(@(tau,y) [sech(opt.beta * tau^opt.n)^2; y(1)], linspace(0,1,0.5+opt.nPoints/2), [0; 0]);

F2vals = soln(:,1);
% N.B. We integrate \int ... d\tau above when we wanted \int ... dt.
% Since d\tau = (2/T_p) * dt we need to rescale:
phases = soln(:,2) * 2 * pi * (opt.Tp/1000) / 2;

phT = [-phT(end:-1:2).' phT.'];
F2vals = [-F2vals(end:-1:2).' F2vals.'];
phases = [phases(end:-1:2).' phases.'];

if ~isequal(size(tau),size(phT)) || max(abs(tau(:) - phT(:))) > 1e-10
    error('Logic error.')
else
    clear phT
end

% I don't understand why F2 doesn't run smoothly from +1 --> -1. Rescale so
% it does.
phases = phases/F2vals(1);
%phases = phases - phases(1); % Not sure here: Should we have integrated from outside-in --> swap over here??
F2vals = F2vals/F2vals(1);

% Scale up according to desired bandwidth
phases = A*1e3*phases;
F2vals = A*F2vals;

phases = phases + 2.0*pi()*opt.freq *(t - t(round(length(t)/2))) / 1000.0 ;

if nargout > 0
    vars = whos();
    for idx=1:numel(vars)
        out.(vars(idx).name) = eval(vars(idx).name);
    end
end

if ~opt.plot
    return
end

figure(5)
clf
plot(t,B1Hs8)
xlabel('time/ms')
ylabel('|B_1|/B_1(0)')
title('|B_1| for Hs8 pulse')

figure(6)
plot(tau,sech(opt.beta*tau))
xlabel('tau')
ylabel('|B_1| for HS pulse')

figure(7)
clf
plot(tau,F2vals)

figure(8)
clf
subplot(1,3,1)
plot(t,B1Hs8)
xlabel('time / ms')
ylabel('B1 / B1(0)')

subplot(1,3,2)
plot((tau+1)*opt.Tp/2,A*F2vals)
xlabel('time / ms')
ylabel('Frequency offset / kHz')

subplot(1,3,3)
plot((tau+1)*opt.Tp/2,A*phases)
hold on
plot((tau+1)*opt.Tp/2,mod(A*phases+pi,2*pi)-pi,'g-')
legend('Phases','Phases wrapped -\pi -- \pi')
xlabel('time / ms')
ylabel('phases')

figure(11)
plot(t,A*phases)
xlabel('time/ms')
ylabel('phases/rad')
title('Phase for HS8 pulse')

figure(12)
plot((tau+1)*opt.Tp/2,mod(A*phases+2*pi,2*pi))
xlabel('time/ms')
ylabel('phases/rad')
title('Phase for HS8 pulse')

figure(9)
clf
plot((tau+1)*opt.Tp/2,B1Hs8.*cos(A*phases),'r-')
hold on
plot((tau+1)*opt.Tp/2,B1Hs8.*sin(A*phases),'g-')
legend('Re (X)','Im (Y)')

figure(10)
clf
plot(B1Hs8.*cos(A*phases),B1Hs8.*sin(A*phases),'b-')
xlabel('B1x')
ylabel('B1y')
title('Like Fig.1b')
