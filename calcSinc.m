function [out] = calcSinc(varargin)
% Matlab reference implementation of BIR4 pulses 
%
% Syntax:
% 'R' , time-bandwidth product
% calcSinc('R' , 20), default is 40
%
% 'Tp' , duration (in ms)
% calcSinc'Tp',20), default is 10
%
% 'zeta' , value for the truncation factor zeta
% calcHsn('beta', acosh(100)), default is 10
%
% 'nPoints' , integer number of datapoints in output
% calcHsn('nPoints', 1000), default is 10001 - MUST BE AN ODD NUMBER
%
% 'plot' , boolean (true or false) whether to display the pulse
% calcHsn('plot', true), default is true
%
% 'freq' , offset frequency of pulse (Hz)
% calcHsn('freq', 100), default is 0Hz

opt = processVarargin(varargin{:});
disp('Check 10.1')

if ~isfield(opt,'n')
    opt.n = 8;
end

if ~isfield(opt,'R')
    opt.R = 40;
end

if ~isfield(opt,'Tp')
    opt.Tp = 10; % Units: ms.
end

if ~isfield(opt,'beta')
    opt.beta = acosh(100); % For 1% truncation level (i.e. 1/1% = 100)
end

if ~isfield(opt,'nPoints')
    opt.nPoints = 10001;
end

if ~isfield(opt,'plot')
    opt.plot = true;
end

isfield(opt , 'freq' )

if ~isfield(opt,'freq')
    opt.freq = 0;
end
disp('Check 10.2')

A = opt.R / opt.Tp;        % I.e. \Delta\omega_{max} in de Graaf's notation. Units of kHz.

tau = linspace(-0.5,0.5,opt.nPoints); % Scaled time.
t   = (tau+1) * opt.Tp; % Time in same units as Tp.
disp('Check 10.3')

B1sinc = sin(2*pi*opt.n*tau)./(2*pi*opt.n*tau);
phases = zeros(length(t));
disp('Check 10.4')

if nargout > 0
    vars = whos();
    for idx=1:numel(vars)
        out.(vars(idx).name) = eval(vars(idx).name);
    end
end
disp('Check 10.5')

if ~opt.plot
    return
end
disp('Check 10.6')

figure(5)
clf
plot(tau,B1sinc)
xlabel('tau')
ylabel('|B_1| for sinc pulse')
disp('Check 10.7')

subplot(1,3,3)
plot(t,phases)
xlabel('time / ms')
ylabel('phases')
disp('Check 10.9')


